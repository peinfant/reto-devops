FROM node:latest

# Creamos el directorio en donde se trabajará
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Instalamos las dependecias
RUN npm install

# Arreglamos los problemas de dependencias
RUN npm audit fix --force

# Copiamos los archivos del proyecto
COPY . .

# El usuario node ya existe
# Le damos permisos al usuario node a nuestra aplicacion
RUN chown -R node.node /usr/src/app

#Utilizamos el usuatio node
USER node

#Se expone el puerto 3000
EXPOSE 3000

# se ejecuta la aplicacion
CMD [ "node", "index.js" ]